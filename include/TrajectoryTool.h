//
// Created by sebastian on 06.07.21.
//

#ifndef SRC_TRAJECTORYTOOL_H
#define SRC_TRAJECTORYTOOL_H

#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

class TrajectoryTool {

public:
    static moveit::planning_interface::MoveGroupInterface::Plan
    invert_plan(moveit::planning_interface::MoveGroupInterface::Plan &original_plan,
                moveit::planning_interface::MoveGroupInterface &move_group, std::string group);

};
#endif //SRC_TRAJECTORYTOOL_H
